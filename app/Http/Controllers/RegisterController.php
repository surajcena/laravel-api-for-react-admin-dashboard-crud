<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Register;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    function register(Request $request){
        $user=new Register();
        $user->name=$request->input('name');
        $user->email=$request->input('email');
        $user->password=Hash::make($request->input('password'));
        $user->save();
        return $user;
    }
    function login(Request $request){
        $user=Register::where('email',$request->email)->first();
        if(!$user || !Hash::check($request->password,$user->password))
        {
            
            return"user email or password didn't matched";
        }
       
            return $user;
        
        
    }
}
